## 说明
fhs封装了一个简单的列表组件，可实现带过滤条件，列表，分页的普通列表功能。    
组件使用JSON驱动，组件文件为 crud.vue 文本只做重要的参数说明，其他的请自己看组件prop声明。
## demo
<img src="/images/list_showcase.jpg"/>
template

```html
<template>
    <base-container>
            <pagex-crud
                    ref="crud"
                    :filters="filters" 
                    :columns="columns"
                    :api="api"
                    :sortSett="sortSett"
            >
            </pagex-crud>
    </base-container>
</template>
```

script

```javascript
    import crudMixins from "@/mixins/crudMixins";
    //这个路径可能需要改下
    import {mapGetters} from "vuex"
    export default {
        name: "logLogin",
        mixins: [crudMixins],
        data() {
            return {
                api: "/basic/ms/logLogin/pagerAdvance",//后台接口
                // 列表排列顺序（更新时间）
                sortSett: [
                    {
                        direction: "DESC",
                        property: "createTime",
                    },
                ],
                columns: [//列表上展示的列
                     {label: '用户', name: 'loginUserName', width: 150},
                     {label: '用户名', name: 'loginName', width: 150},
                     {label: '时间', name: 'createTime'},
                     {label: 'ip', name: 'ipAddress', width: 150},
                     {label: 'ip信息', name: 'ipInfo', width: 150},
                     {label: '浏览器', name: 'browser', width: 150},
                     {label: '操作系统', name: 'os', width: 150},
                     {label: '状态', name: 'stateName', width: 150},
                     {label: '类型', name: 'typeName', width: 150},
                     {label: '错误类型', name: 'errorInfoName'}
                ],
                filters:[ // 过滤条件
                     {"name":"ipAddress","label":"ip","type":"text","operation":"like"}, // like代表模糊匹配
                     {"name":"loginName","label":"用户名","type":"text","operation":"like"},
                ]
            };
        },
        created() {},
        computed: {
            ...mapGetters(["user"]),
        },
        methods: {

        },
    };
```
## 自定义按钮
属性：buttons    

demo，点击新增打开dialog，并且设置新增的组织机构id为当前选中组织机构
```json
[
        {
          title: "新增",
          type: "primary",
          size: "mini",
          isRight : true,// 把按钮显示到列表右上角，默认是左上角
          icon: "el-icon-plus",
          permission: ['sysUser:add'],//权限编码
          click: (_row) => {//点击事件
            this.title ='新增';
            this.$set(this, "init", {organizationId:this.org.id})
            this.isEdit = false;
            this.open = true
          }
        },
      ]
```
## 过滤条件
属性：filters

用于设置列表上面的过滤条件，支持text 输入框，select下拉框 treeSelect 下拉树  date-picker 日期区间 datetimerange 时间区间。    

demo：

```json
[
        {
          formname: "姓名",
          name: "userName",
          type: "text",
          operation: "like",// like代表模糊匹配 支持 = > < >= <= between in not in 等 默认是= 
        },
          {
            formname: "统计周期",
            name: "period",
            type: "select",
            dictCode: "period",//字典码
            operation: "="
        },
          {
            formname: "报警时间",
            name: "date",
            type: "date-picker",//日期区间
            operation: "between",
            value: []
          },
          {
            formname: "行政区域",
            name: "areaId",
            type: "treeSelect",
            placeholder: '请选择',
            httpMehod: "POST",
            api: process.env.VUE_APP_BASIC_API + "/ms/area/selectAreaTree", //自定义接口下拉数据
            queryFilter: {}
          },
      ]
```
## 列设置
列属性：columns    
列有几种类型，selection 多选框，index 序号 formart 允许自定义格式(比如列文字显示红色)  popover 带tips textBtn 操作按钮    
demo：

```json
 [
        {label: "序号", name: "index", type: "index", width:'60', fixed: 'left'}, //列锁定
        {label: '用户', name: 'loginUserName', type: 'popover', width: 150, fixed: 'left'},
        {label: '用户名', name: 'loginName', type: 'popover', width: 150, fixed: 'left'},
          {
            label: '分组编码', name: 'groupCode', type: 'formart',
            formart: "<label style='cursor:pointer'>${groupCode}</label>",//格式化代码
            click: function (_row) {//点击事件
                  this.$router.push({path: '/dict/type/data/',query:{groupCode: _row.groupCode}});
            }
        },
        {
          label: "操作",
          name: "operation",
          type: "textBtn",
          width: "240",
          fixed: 'right',
          textBtn: [
            {
              title: "编辑",
              type: "bottom",
              size: 'mini',
              click: (_row, name) => {
                  this.$set(this, 'init', _row)
                  this.title = '编辑';
                  this.open = true;
                  this.isEdit = true;
              }
            },
            {
              title: "删除",
              type: "danger",
              api: '/basic/ms/dictItem/', //框架自带删除事件
              size: 'mini',
              idFieldName: 'dictId'
            }
          ]
        }
      ],
```
## 接口和参数
属性：api 列表数据后台接口地址，methodType 接口http method 默认post，paramsQuery 给后台的扩展参数，querys 自带参数 sortSett排序。
demo 场景：我查询orgid为1的用户列表(假如左侧组织机构tree选中了某个org)，根据创建时间倒叙排列
```json
{
  api: "/basic/ms/sysUser/pagerAdvance",
  sortSett: [ //默认排序
    {
      direction: "DESC",//ASC 代表正序，DESC代表倒叙 即从大到小
      property: "createTime",//排序字段
    },
  ],
  querys: [
    {
      operation: "=",
      property: "organizationId",
      relation: "AND",
      value: 1,
    }
  ]
}
```
## 其他

| 序号  | 属性名称   | 属性用途  |
| ------------ | ------------ | ------------ |
| 1  | isNeedPager  | 是否需要分页  |
| 2  | tableList  | 不请求后台直接给table的数据  |
| 3  | isAdvancePager  | 无total分页开关，可以加快后台查询速度  |
| 4  | height  | 高度  |
| 5  | namespace  | 命名空间，同命名的pagex-dialog page-form可以在提交表单后自动关闭  |
| 6  | minPageSize  | 最小的pagesize 默认是10  |
| 7  | lineClick  | 给true的话会触发@rowClick事件 |

