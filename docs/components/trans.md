## 1、type
指定翻译的数据源    
easy trans支持多种数据源，所以要手动指定。
可选值：
- TransType.AUTO_TRANS  自动自定义数据源翻译，左侧有单独的章节讲解如何自定义数据源。
- TransType.DICTIONARY  字典缓存为数据源
- TransType.SIMPLE 本微服务/单体项目系统自动查表作为数据源
- TransType.RPC 自动调用其他微服务接口进行自动查表作为数据源
## 2、key
指定具体数据源(type为TransType.AUTO_TRANS或者TransType.DICTIONARY才需要配置)    
- type为TransType.DICTIONARY  传入字典分组编码 比如orderStatus
## 3、ref
将翻译出来的数据映射到本pojo的某个属性上    
比如根据userId翻译userName，vo中已经有userName了（必须手动在vo定义username属性，否则翻译失败），可以通过ref对vo的username进行赋值。
## 4、refs
类似ref，不过支持多个，是数组格式 不是字符串逗号分隔哦

 ----推荐使用平铺模式来代替ref和refs(详情见左侧平铺模式)----

## 5、target
TransType.SIMPLE的时候指定对方的po类    
比如是userId那么就指定UserPo.class
## 6、targetClassName
TransType.RPC的时候指定对方的po类全名  @since 2.2.12 TransType.SIMPLE类型也支持  
com.ucenter.po.User
## 7、fields
用来指定我需要对方类的要哪个字段(TransType.SIMPLE和TransType.RPC可用)    
支持指定多个，数组格式，不是字符串分隔哦
## 8、alias
别名，解决翻译结果字段名重名问题    
一个vo有createUserId和updateUserId 他们的结果都叫userName 就可以通过别名来区分，比如updateUserId配置了update 那么结果就为updateUserName。
## 9、serviceName
微服务名称(TransType.RPC 有效)  
TransType.RPC的时候组件自动调用其他微服务的接口获取数据，所以要指定服务名称，easyTrans会自动去注册中心获取对应的服务实例URL进行调用。

## 10、serviceContextPath 
服务的context-path    
如果远程微服务配置了context-path 可以通过此属性来配置，保证RPC调用不会404


## 11、 uniqueField
vo中的属性不是对方的id，而是唯一键，这里配置target类唯一键的字段名。    
TransType.RPC 和TransType.SIMPLE 可用。    
注意：不支持同一个类中混用id和唯一键，比如：Student类中有createUserId 和 updateUserMobile（用户的唯一键字段）  

## 12、dataSource
数据源名称    
在多数据源环境下指定此翻译使用哪个数据源 TransType.SIMPLE 有效。

## 13、sort
翻译顺序    
比如 文章表有createBy 字段 对应用户表  用户表有orgid字段对应 组织表。
文章vo需要展示创建人以及创建人所在部门。 就可以指定sort 先对createBy字段进行翻译，取orgid用ref设置到文章vo上，然后再用orgid翻译org的名称。



 