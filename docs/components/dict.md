## 1  术语
假如有以下数据，第一列我们称为 字典分组，第二列称为字典码，第三列称为字典描述   
gender,0,男
gender,1,女
orderStatus,0,创建
orderStatus,1,已支付
orderStatus,2,已评价
##  2 初始化
你可以在系统启动的时候初始化字典，字典的数据在哪获取easyTrans不关心，json也好，properties也好，微服务也好，查数据库也好都可以。
 ``` java
  @Autowired
  private  DictionaryTransService dictionaryTransService;

  //微服务模式下需要用到redis  请设置  easy-trans.dict-use-redis:true 和  easy-trans.is-enable-redis:true
   

  Map<String,String> transMap = new HashMap<>();
  transMap.put("0","男");
  transMap.put("1","女");
  dictionaryTransService.refreshCache("gender",transMap);

  //如果字典修改了 需要刷新所有微服务的字典缓存 请调用：
   dictionaryTransService.refreshCacheAndNoticeOtherService("gender",transMap);
 ```
##  3 国际化
有的时候我们的系统需要国际化，字典国际化可以按照以下写法来初始化数据。
  ``` java
  @Autowired
  private  DictionaryTransService dictionaryTransService;
   //字典国际化支持
  Map<String,String> transMap = new HashMap<>();
  // 如果不要国际化则是  ransMap.put("0","男");  transMap.put("1","女");
  transMap.put("0_zh_CN","男");
  transMap.put("1_zh_CN","女");
  transMap.put("0_en","boy");
  transMap.put("1_en","girl");
  dictionaryTransService.refreshCache("gender",transMap);
  //开启国际化支持，不要国际化的可以去掉 getLanguageTag 可以在spring里取
  dictionaryTransService.openI18n(new LocaleGetter(){
             @Override
            public String getLanguageTag() {
                Locale locale = LocaleContextHolder.getLocale();
                return locale.toLanguageTag();//springboot可以国际化可以通过这段代码告诉框架当前语言环境
            }
 });
 ```
## 4 数据刷新
  ``` java
//刷新整组字典
dictionaryTransService.refreshCacheAndNoticeOtherService(String dictGroupCode, Map<String, String> dicMap);
//刷新单个字典项
dictionaryTransService.refreshDictItem("gender","1","girl");
//删除字典分组
removeDictGroupAndNoticeOtherService("gender");
 ```

