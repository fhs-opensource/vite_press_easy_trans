## 功能说明
2.2.1 版本起新增特性    
支持把POJO中的名字(张三)翻译为张三的id，字典 男 翻译为 字典码0；    
完全支持：mysql，postgresql     
不完美支持：sqlserver (CONCAT 字段在where 后面过滤不出来数据)    
其他数据库： 理论上支持CONCAT 关键字就可以，支持自定义扩展。
## 添加依赖
注意：从2.3.0和3.1.0 开始groupId统一改为了org.dromara，在此之前的版本保持com.fhs-opensource
```xml
<dependency>
    <groupId>com.fhs-opensource</groupId>
    <artifactId>easy-trans-untrans-driver</artifactId>
    <version>2.2.1以上版本都支持</version>
</dependency>
```
## 字典使用
```java
private String genderName = "男;

@UnTrans(type=UnTransType.DICTIONARY,dict="gender",refs = {"genderName"})
private Integer gender;
```
## SIMPLE(自动联表)
   
1、根据某个字段查询其主键/唯一键 赋值
```java
private String schoolName;
@UnTrans(type= UnTransType.SIMPLE,tableName = "school",refs = "schoolName",columns = "school_name",uniqueColumn = "id")
private Integer schoolId;
```
2、根据多个字段(组合唯一键) 查询主键赋值
```java
private String studentName;
private String studentAge;
@UnTrans(type= UnTransType.SIMPLE,tableName = "t_user",refs = {"studentName","studentAge"},columns = {"name","age"},uniqueColumn = "user_id")
private Integer userId;
```
3、多表怎么搞？    
tableName 可以自己写join  比如 t_company c join  t_org o on o.company_id = c.id
然后columns 给 ["c.name","o.name"]

## 手动翻译
```java
transService.unTransMore(list);
transService.unTransOne(pojo)
```
## 可能出现的坑
1、 多数据源兼容问题。    
2、 带事务的情况下,反向翻译用的connection非spring 事务管理器获取的connection。    
3、 其他问题请加群联系作者反馈。

## 兼容性说明
框架会自动拼接以下sql：    
SELECT CONCAT(name,'/',age) AS groupKey,user_id as uniqueKey FROM t_user WHERE CONCAT(name,'/',age) IN ('小明/18')    
如果你使用的db支持这种写法，则可以支持，如果不支持这种写法则不支持多字段联合查询，单个字段反向翻译的sql是标准sql如下：    
select column from table where column in (xx)
## 扩展
如果你的数据库不支持CONCAT，可以通过自定义SimpleTransService.SimpleUnTransDiver来扩展，可以参考：
easy-trans-untrans-driver模块的CommonUnTransDriver来进行扩展。
## FAQ
反向翻译不起作用检查2点    
1 部分包引入的org.dromara部分包引入的是com.fhs-opensource，会出问题    
2 虽然引入了untrans-driver 但是idea的bug导致没有识别到依赖    
终极解决方法：UnTransDriverConfig 找到这个类，打断点，看看进不进断点，进了断点不好使是我的bug，没进断点自己排查