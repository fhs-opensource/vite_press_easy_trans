## 介绍
通过简单的注解，来实现软删除表的重复数据校验。
## 使用
需要2个注解，第一个是@NotRepeatDesc() 第二个是 @NotRepeatField    
比如同一个父组织机构下面的子组织不能重名，比如xx分公司不能有2个财务部，但是不同的分公司可以各有一个财务部。
```java
@NotRepeatDesc("同一个父部门下的子部门名称不可重复")
public class UcenterMsOrganizationPO extends BasePO<UcenterMsOrganizationPO> implements Treeable {
    @NotRepeatField
    private String name;

    @NotRepeatField
    private String parentId;
}

```
