## 1、配置讲解
-  easy-trans.is-enable-tile:true   开启平铺模式(transMap的key平铺到和vo一个级别)
-  easy-trans.is-enable-map-result  开启map自动翻译(ruoyi的ajaxResult 可以使用此配置)
-  easy-trans.dict-use-redis 字典缓存是否放到redis中 做二级缓存，微服务模式推荐开启
- easy-trans.is-enable-global 开启responseBody 全局拦截后自动翻译
- easy-trans.is-enable-redis  开启redis支持 微服务模式推荐使用redis
- easy-trans.db-type  数据库类型，反向翻译时使用，目前仅支持mysql
- easy-trans.is-enable-custom-rpc 控制RPC翻译的范围，标记@RpcTrans注解的PO才能被RPC翻译 默认为false
- easy-trans.mp-new Mybatis-plus 为 3.5.3.2版本以上的3.x 版本请设置为true
- easy-trans.is-enable-cloud 默认为true 如果前期先单体跑，后期可能改为springcloud微服务，可以先设置为false，后期设置为true，设置为false RPC翻译会走SIMPLE的逻辑
## 2、不同的翻译类型
###  2.1 类型如何选择
@Trans(type=?)  问号应该怎么写？
- 字典码翻译  TransType.DICTIONARY
- 其他表主键/唯一键翻译  TransType.SIMPLE
- 其他表主键/唯一键翻译 但是 但是  这个表在其他的微服务上   TransType.RPC
- 自己写代码做翻译的数据源  TransType.AUTO_TRANS
- 有多个模块，但是会打包到一个jar运行，各个模块之间的PO无法相互依赖 TransType.SIMPLE  使用targetClassName来制定对方PO的类全名

###  2.2 实战
   ```java
   
 // 字典翻译 
@Trans(type = TransType.DICTIONARY,key = "sex")
private Integer sex;

//SIMPLE 使用schoolId 获取 schoolName
@Trans(type = TransType.SIMPLE,target = School.class,fields = "schoolName")
private String schoolId;

//RPC 使用其他微服务的school表的id获取 schoolName 并且指定别名 
@Trans(type = TransType.RPC,targetClassName = "com.fhs.test.pojo.School",fields = "schoolName",serviceName = "easyTrans",alias = "middle")
private String middleSchoolId;

```
@Trans的详细使用请点击左侧@trans注解详解章节


## 3、自动翻译和手动翻译
- easy-trans.is-enable-global 设置为true的时候，所有的responseBody框架都会拦截尝试翻译，支持嵌套。称之为自动翻译。
- 可以注入 TransService 然后调用 transOne或者transBatch 来翻译一个或者多对象，称之为手动翻译，手动翻译不支持平铺。
- 使用@TransMethodResult 标记方法，框架会自动翻译方法return的值  基于AOP实现。
- 开启全局翻译后 如果某个controller的返回值不需要返回结果可以 添加@IgnoreTrans 到方法上

```java
//手动翻译
@Autowired
private TransService transService;
transService.transOne(school);
transService.transBatch(schoolList);

//标记方法结果翻译
@TransMethodResult
public Student getStudent(){
        Student student = new Student();
        student.setTeacherId(1);
        return student;
}

//忽略翻译
@IgnoreTrans
@GetMapping("/one")
public HttpResult<Student> student(){
	 return HttpResult.success(new Student());
}
```

## 4、多个翻译
多个翻译指的是字段的value 是多个，比如List,Set,数组,逗号分隔的字符串等等。    
字典，RPC，SIMPLE 均支持多个翻译，不需要单独写什么，框架会判断如果value.toString 后带逗号就会走多个翻译的逻辑。

## 5、忽略翻译
如果是返回tree格式数据的接口，强烈建议手动翻译，然后使用忽略翻译注解忽略此接口的自动翻译，忽略翻译可在controller的方法上标记@IgnoreTrans注解。

## 6、参考文章
此文章包含了每一种翻译的常规用法，可为入门参考使用：    
https://blog.csdn.net/csdn_0921/article/details/128380310