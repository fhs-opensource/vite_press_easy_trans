## 1、缓存开启
###     1.1 字典缓存
字典默认使用hashmap来存放缓存，我们也支持使用redis 来组二级缓存，这样内存中没有的时候会自动去redis里面找，然后放到内存中，解决了多微服务字典问题，也保证了性能。    
开启方式：
  ```yaml
easy-trans:
   dict-use-redis : true 
```
###     1.2 SIMPLE 翻译缓存
可以使用@TransDefaultSett 来开启SIMPLE的缓存功能，只需要给数据源PO类上加@TransDefaultSett(isUseCache = true) 即可。    
可选参数：
1. isUseCache 是否开启 缓存 默认为false 需要开启设置为true即可
2. cacheSeconds 缓存失效时间 单位秒  默认5
3. maxCache 最大缓存数量 默认为1000
4. isAccess  默认false  设置为true的话会按照最后一次访问时间 进行缓存过期计时 false按照添加时间计时

###     1.3 RPC 翻译缓存
在配置类上添加:
```java
         @Autowired
         private TransCacheManager transCacheManager;
         transCacheManager.setRpcTransCache("com.fhs.test.pojo.School",
                  SimpleTransService.TransCacheSett.builder().cacheSeconds(20).maxCache(1000).build());
```
TransCacheSett 类同样包含 cacheSeconds、 maxCache、 isAccess 3个参数。
上面这段代码要配置到消费者对应微服务中，比如Order中要翻译userid order和user是2个微服务，要把代码放到order微服务里，而不是user微服务。    
SimpleTransService.TransCacheSett 使用build构造的时候默认值不起作用，会导致缓存配置不起作用，所以cacheSeconds和maxCache都要手动指定。

## 2、缓存刷新/清理
###     2.1 字典缓存
在微服务模式下，如果修改了某个字典，可以通过以下方式刷新各个微服务字典：
  ```java
        Map<String,String> transMap = new HashMap<>();
        transMap.put("0","男");
        transMap.put("1","女");
        dictionaryTransService.refreshCacheAndNoticeOtherService("gender",transMap);
```
当执行了上面的代码后，easy trans会刷新redis的缓存，然后清理掉各个微服务的内存缓存 重新获取redis中的缓存。

###     2.2 SIMPLE RPC 翻译缓存清理
```java
        @Autowired
        private TransCacheManager transCacheManager;
        transCacheManager.clearCache(User.class,"1");
```
通过上述代码即可清理掉本微服务和其他微服务的所有跟User相关的SIMPLE和RPC缓存。
PS：如果要清理掉其他微服务的RPC缓存，需要使用Redis。因为我用redis做了广播去清理。

## 3、uniqueField 使用缓存
注意：暂时不支持uniqueField 和 pkey相同字段 比如 主键有个1  uniqueField  还有一个1 他们代表不同的数据。  
通过以下方式来配置User除了主键外 还要根据身份证号码字段进行缓存匹配
```java
transCacheManager.setUniqueFieldCache("com.xx.User","idNo");
```