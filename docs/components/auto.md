## 1、介绍
TransType.AUTO_TRANS 用于自己写代码实现翻译数据源，Easy trans不在负责查询DB。

## 2、数据源提供

AutoTrans 注解标记到数据源提供类上，提供一些默认配置，参数如下：

|  序号 |属性   |默认值   | 备注   |
| ------------ | ------------ | ------------ | ------------ |
|  1 |  namespace |   | 与@Trans(key="xx")对应  |
| 2  | fields  |   |  返回的对象中哪些字段需要做翻译结果 |
|  3 | defaultAlias  |   | 别名，比如Teacher表的name字段如果配置了teacher，翻译结果为：teacherName  |
|  4 | globalCache  | false  |全局缓存，默认关闭   |
|   5| isAccess  |  false |开启全局缓存后，缓存失效时间计算规则，配置false按照缓存加入时间算，true按照最后一次访问时间算   |
|   6| cacheSeconds  | 1  |  缓存时间，默认1秒 |
|   7| maxCache  |  1000 | 最大缓存对象数量  |

数据提供类需要实现AutoTransable接口，包含3个方法：selectByIds用于查询集合，selectById用于查询单个对象，select用于查询全部对象。    
select 是历史设计遗留问题，现在基本用不上，做个空实现即可，后面版本会处理掉这个方法。

   ```java
@Service
@AutoTrans(namespace = "teacher",fields = {"name","age"},
        defaultAlias = "teacher",globalCache = true,cacheSeconds = 10,maxCache = 100)
public class TeacherService implements AutoTransable {
    @Override
    public List selectByIds(List ids) {
        List list = new ArrayList();
        Teacher t  = null;
        for (Object id : ids) {
            t  = new Teacher();
            t.setTeacherId(id.toString());
            t.setName("老师名字" + id);
            t.setAge(18);
            list.add(t);
        }
        System.out.println("teacher service的findByIds 进来了");
        return list;
    }

    @Override
    public List select() {
	    // 一般用不到这个，后面版本添加默认实现。
        return new ArrayList<>();
    }

    @Override
    public VO selectById(Object primaryValue) {
        System.out.println("teacher service的selectById 进来了" + primaryValue);
        Teacher t  = new Teacher();
        t.setTeacherId(primaryValue.toString());
        t.setName("老师名字" + primaryValue);
        t.setAge(18);
        return t;
    }
}

```

## 3、最后
自定义翻译在作者自己的项目中越来越少 所以此处部分设计不是很合理，大家多多提建议，作者从善如流，比较勤快。


