
## 什么是级联翻译
vo只有区县id,不级联只能拿到区县名称，通过级联就可以拿到地市名称，省份名称。    
下面是一个手敲的例子，大家理解下：

```java

class Area{
    int id;
    String name;
    @Trans(type=simple,target=City.class,fields={"cityName","provinceId"},refs={"cityName","provinceId"},sort=0)
    int cityId;
    String cityName;
    @Trans(type=simple,target=Province.class,fields={"provinceName"},refs={"provinceName"},sort=1)
    int provinceId;
    String provinceName;
}

```

上述代码，表里只有cityId字段，但是可以拿到provinceName给前端展示，称之为级联。


## 级联不成功常见问题
1、@Trans注解的sort配置错误，先翻译的要把值设置小一些，后翻译的设置大一些    

