在项目中我们可能链接多个数据库，比如我有订单和用户2个库，user表在用户库里，order表在订单库里，我需要翻译订单的用户id为用户姓名，就会用到跨库查询。   
1、介于目前实现数据库切换的spring组件很多，每种切换的api都不相同，所以我们封装了DataSourceSetter接口，大家各自实现一个实现类来实现切换数据源的代码。   
示例：

```java
/**
 * 数据源设置
 */
@Component
public class MyDataSourceSetter implements DataSourceSetter{

    /**
     * 设置数据源
     * @param datasourceName 数据源名称
     */
   public void setDataSource(String datasourceName){
       //切换数据源的方法
   }
}

```
2、在配置文件中开启多数据源支持

```
easy-trans.multiple-data-sources=true
```
3、在使用@Trans注解的时候添加 dataSource （数据源名称）设置

```java
@Trans(type = TransType.SIMPLE,target= User.class, alias = "createUser",fields = "userName",dataSource = "basic")
private Long createUser;

```