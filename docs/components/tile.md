
不开启平铺：
```
 {name:"zhangsan",friendUserId:1,transMap:{friendUserName:"小明"}}
```
开启平铺
```
 {name:"zhangsan",friendUserId:1,friendUserName:"小明"}
```
开启方式：
```
easy-trans.is-enable-tile=true
```
想了解原理的可以跟踪：EasyTransResponseBodyAdvice 这个类看实现原理。