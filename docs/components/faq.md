
## SimpleTransService错误
引入starter后根据自己的ORM框架引入扩展包即可
## 看前端返回json没有被翻译
关闭了全局翻译后需要手动在controller方法上加注解才可以，具体看文档《基础使用章节》   
## RPC翻译 unkonwhost
因为自定义了resttemplate，没有加@LoadBalanced注解导致的
## Redis看字典的缓存key带乱码
自定义redistemplate设置key的序列化方式即可，具体百度。
## 找不到XX类ID注解问题
手动在POJO中定义一个Map<String, String> transMap=new HashMap<>();即可不需要ID注解。    
注意：高版本的map泛型为：Map<String, Object> transMap=new HashMap<>();
## Unable to make field transient 
改为手动翻译，使用@IgnoreTrans忽略某个方法的自动翻译
## No value specified for 'Date'
beanutils 的问题 apache 相关包升级到 1.9.x 即可
## bean searcher 分页结果无法翻译
升级到2.1.11版本之后即可
##  is not known to the MybatisPlusMapperRegistry.
去掉spring-boot-devtools 依赖 这货会让同一个class在不同的类加载器中加载多次，导致报上面的问题
## 存在多个restTemplate 启动失败
通过 primary 注解标记一个主要的restTemplate，多个redistemplate的报错也这么解决

## ref和refs 在翻译集合的时候字段为空
升级到2.1.12 以及以后的版本即可

## MybatisPlusException xx Not Found TableInfoCache.
请确保po对应的mapper继承了baseMapper
## Mybatis plus 3.5.3.2 以上版本 找不到某个方法的问题
升级easy-trans 2.2.5 以上版本并且设置easy-trans.mp-new: true 这样就会走querywrapper的select(List) 方法

## 翻译结果数据错乱.
比如字典里1对应的待付款但是给翻译成已付款了，这种情况一般是被翻译的vo集合中存在主键为null或者主键重复的情况。    
解决方法同：找不到XX类ID注解问题

## Simple翻译不出结果来.
1、target要写PO的class，不要写成vo或者dto了，就是你加@tablename 或者@table注解的那个pojo。    
2、如果2个表都有相同的字段 比如 user也有name teacher也有name 导致name重复了有一个name翻译不出来，可以使用别名来处理。    
3、simple方式一般会调用ORM的能力去查数据库，所以观察sql有助于定位问题。
## 字典翻译不了.
1、第一个断点到TransService的trans方法如果不进则看下是否手动翻译或者是否开启全局翻译，部分复杂的vo去全局翻译可能会失效，可尝试手动翻译。    
2、第二个断点到DictionaryTransService的transOne方法，如果不进看看翻译类型是否写错了。    
3、第三个断点到BothCacheService的get方法，看看字典是否从本地map或者redis中能取到值，取不到值定位下有没有把数据放到dictionaryTransService中。
4、trans注解的ref属性是否指向了一个不存在的pojo属性

## Easy Query如何标识主键
使用Id4Trans注解

## 部分数据无法翻译
请检查一次返回的对象中是否存在id相同的数据，比如有2个user 他们的id是相同的，这种情况请手动翻译，然后在controller方法标记@Untrans注解