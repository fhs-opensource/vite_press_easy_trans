比如系统90%使用userId 翻译的时候都需要userName和mobile 2个字段，可以使用@TransDefaultSett 注解到PO类上提供默认配置。

| 序号  | 属性  | 默认值   | 备注  |
| ------------ | ------------ | ------------ | ------------ |
| 1  | defaultFields  | {}  |  翻译字段 |
|  2 | defaultAlias  |   |  别名 |
|  3 |  uniqueField |   | 唯一键字段名  |
|  4 | dataSource  |   | 数据源名称  |
|  5 | isAccess  |  false |  缓存过期策略，true按照最后访问时间计算，false按照缓存时间计算  |
| 6  |  cacheSeconds | 5  |  缓存时间，单位秒 |
|  7 | maxCache  | 1000  |  最大缓存个数 |
| 8  |  isUseCache |  false |  是否使用缓存 |
