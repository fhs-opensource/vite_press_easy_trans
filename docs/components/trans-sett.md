## 说明
比如userController 的 list接口和info接口都复用了UserVo，UserVo中包含A,B 2个字段需要翻译。    
list接口只需要A字段的翻译结果，不需要翻译B字段，info接口需要翻译A,B 2个字段。
## TransSett注解
@TransSett(exclude = {"B"},include = {"A"}) 加到controller方法上即可。    
exclude和include配置一个就可以了。exclude是排除哪些字段，剩下的字段都翻译，include是只翻译哪几个，其他的不翻译。    
如果2个属性都配置了，以include为准。