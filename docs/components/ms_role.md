## 功能权限
我们的权限设置模型为 菜单->权限   用户->角色->权限，可以把功能权限控制到api和按钮级别。    

每个菜单都有自己的命名空间，假如是user，我们给用户菜单下面添加了增删改查4个权限，那么添加权限就是user:add,修改是user:update


我们使用了sa-token来做登录和控制权限，所以我们可以给方法上添加@SaCheckRole注解控制api权限。
```java
@SaCheckRole("sysUser:del")
@DeleteMapping("/{id}")
@ApiOperation(value = "删除-vue专用")
@LogMethod(type = LoggerConstant.METHOD_TYPE_DEL, pkeyParamIndex = 0)
public HttpResult<Boolean> del(@ApiParam(name = "id", value = "实体id") @PathVariable String id, HttpServletRequest request) {
        if (isPermitted(request, "del")) {
           baseService.deleteById(id);
           return HttpResult.success(true);
        }
        throw new NotPremissionException();
}
```
## 数据权限
我们给t_ucenter_ms_role表预留了data_permissions字段，可以通过二开来存放角色数据权限。    
让框架去加载数据权限可以使用自定义DataPermissionLoader来实现。

```java
/**
* 数据权限加载器
  */
  @Component
  public class OfplDataPermissionLoader implements DataPermissionLoader {

  @CreateCache(expire = 1800, name = "user:data_permission", cacheType = CacheType.BOTH)
  private Cache<Object, Map<String, Set<String>>> permissionCache;

  @Override
  public Map<String, Set<String>> loadDataPermissions(UcenterMsUserVO ucenterMsUserVO, String token) {
  Map<String, Set<String>> result = new HashMap<>();
  result.put("parkIds", new HashSet<>(Arrays.asList("1", "2", "3")));
  return result;
  }
  }
```
把数据权限和用户token绑定后，我们可以通过自定义baseservice类来实现数据权限控制(让需要控制数据权限的service继承你的service)。    
下面给大家一个示例，通过station_id来控制数据权限。
```java
public abstract class OfplBaseServiceImpl<V extends VO, P extends BasePO> extends BaseServiceImpl<V,P> {


    /**
     * 可序列化
     */
    private static final int FLAG_SERIALIZABLE = 1;

    @Override
    public V selectOneMP(Wrapper<P> queryWrapper) {
        return super.selectOneMP(initPermissionDataFilterOwner(queryWrapper));
    }

    @Override
    public Long selectCountMP(Wrapper<P> queryWrapper) {
        return super.selectCountMP(initPermissionDataFilterOwner(queryWrapper));
    }

    @Override
    public List<V> selectListMP(Wrapper<P> queryWrapper) {
        return super.selectListMP(initPermissionDataFilterOwner(queryWrapper));
    }

    @Override
    public List<Map<String, Object>> selectMapsMP(Wrapper<P> queryWrapper) {
        return super.selectMapsMP(initPermissionDataFilterOwner(queryWrapper));
    }

    @Override
    public List<Object> selectObjsMP(Wrapper<P> queryWrapper) {
        return super.selectObjsMP(initPermissionDataFilterOwner(queryWrapper));
    }

    @Override
    public IPage<V> selectPageMP(IPage<P> page, Wrapper<P> queryWrapper) {
        return super.selectPageMP(page, initPermissionDataFilterOwner(queryWrapper));
    }

    /**
     * 初始化过滤条件
     *
     * @param queryWrapper
     */
    protected Wrapper<P> initPermissionDataFilterOwner(Wrapper<P> queryWrapper) {
        //如果是querWrapper
        if (queryWrapper instanceof QueryWrapper) {
            initPermissionDataFilter((QueryWrapper) queryWrapper);
        } else if (queryWrapper instanceof LambdaQueryWrapper) {
            initPermissionDataFilter((LambdaQueryWrapper) queryWrapper);
        }
        return queryWrapper;
    }

    /**
     * 初始化过滤条件
     *
     * @param queryWrapper
     */
    protected void initPermissionDataFilter(QueryWrapper queryWrapper) {
        UcenterMsUserVO sessionuser = UserContext.getSessionuser();
        queryWrapper.in("station_id",
        UserContext.getDataPermission().get(OfplConstant.PERMISSION));
    }

    /**
     * 初始化过滤条件
     *
     * @param queryWrapper
     */
    protected void initPermissionDataFilter(LambdaQueryWrapper<P> queryWrapper) {
        queryWrapper.in(getSFunction("getStationId",String.class), UserContext
        .getDataPermission().get(OfplConstant.PERMISSION));

    }

    private Map<String, SFunction> functionMap = new HashMap<>();

    /**
     * 获取一个sfunction
     * @param getFunName
     * @return
     */
    private SFunction getSFunction(String getFunName,Class methodReturnType) {
        if(functionMap.containsKey(getFunName)){
            return functionMap.get(getFunName);
        }
        SFunction func = null;
        final MethodHandles.Lookup lookup = MethodHandles.lookup();
        MethodType methodType = MethodType.methodType(Integer.class,getPoClass());
        final CallSite site;
        try {
            site = LambdaMetafactory.altMetafactory(lookup,
                    "invoke",
                    MethodType.methodType(SFunction.class),
                    methodType,
                    lookup.findVirtual(getPoClass(), getFunName, MethodType.methodType(methodReturnType)),
                    methodType, FLAG_SERIALIZABLE);
            func = (SFunction) site.getTarget().invokeExact();
            functionMap.put(getFunName,func);
            return func;
        } catch (Throwable e) {
            log.error("获取" + getFunName + "方法错误", e);
            throw new ParamsInValidException("获取" + getFunName + "方法错误");
        }
    }
}
```
复杂查询需要大家手动控制权限，这里就不赘述了。
