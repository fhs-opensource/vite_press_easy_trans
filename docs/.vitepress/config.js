export default {
    title: 'Easy-Trans',
    description: 'Easy_trans Easy-Trans EasyTrans',
    lang: 'zh-cn',
    themeConfig: {
        nav: [{
            text: '首页',
            link: '/',
        },
            {
                text: '文档',
                link: '/components/readme',
            },
            {
                text: "友情链接",
                items: [
                    {text: 'Iparking停车系统', link: 'https://gitee.com/fhs-opensource/iparking'},
                    {text: 'Mybatis Plus', link: 'https://mp.baomidou.com/'},
                    {text: '小诺开源技术', link: 'https://xiaonuo.vip/'},
                    {text: 'Bean Searcher', link: 'https://bs.zhxu.cn/'},
                    {text: '陕西小伙伴网络科技', link: 'http://www.cptcsoft.com/'},
                ]
            }
        ],

        // 侧边栏
        sidebar: {
            '/components/': [
                {
                    text: "文档",
                    items: [
                        {
                            text: "介绍",
                            link: "/components/readme"
                        },
                        {
                            text: "基础使用(必读)",
                            link: "/components/basic"
                        },
                        {
                            text: "Trans注解详解(必读)",
                            link: "/components/trans"
                        },
                        {
                            text: "缓存使用",
                            link: "/components/cache"
                        },
                        {
                            text: "微服务",
                            link: "/components/micro-service"
                        },
                        {
                            text: "字典详解",
                            link: "/components/dict"
                        },
                        {
                            text: "多数据源",
                            link: "/components/datasource"
                        },
                        {
                            text: "SIMPLE默认配置",
                            link: "/components/simple-default-sett"
                        },
                        {
                            text: "平铺模式",
                            link: "/components/tile"
                        },
                        {
                            text: "级联翻译",
                            link: "/components/cascade"
                        },
                        {
                            text: "AUTO_TRANS使用",
                            link: "/components/auto"
                        },
                        {
                            text: "反向翻译",
                            link: "/components/untrans"
                        },
                        {
                            text: "指定字段",
                            link: "/components/trans-sett"
                        },
                        {
                            text: "常见问题",
                            link: "/components/faq"
                        }]
                }]
        },
        footer: {
            message: 'Apache 2.0 Licensed<a href="http://beian.miit.gov.cn" target="_blank" data-v-42913a4d=""> \n' +
                '陕ICP备2022006153号-2 </a>',
            copyright: 'Copyright © 2019-2023 陕西小伙伴网络科技有限公司'
        },
        editLink: {
            pattern: 'https://gitee.com/fhs-opensource/vite_press_easy_trans/tree/master/docs/:path'
        },
        socialLinks: [
            { icon: 'github', link: 'https://gitee.com/dromara/easy_trans' }
        ],
    },
    markdown: {
        config: (md) => {
            const {
                demoBlockPlugin
            } = require('vitepress-theme-demoblock')
            md.use(demoBlockPlugin)
        }
    }
}
