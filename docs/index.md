---
layout: home
hero:
  name: Dromara Easy-Trans
  text: 一个注解搞定数据翻译,减少30%SQL代码量
  image:
    src:  /images/logo.png
    alt: Dromara Easy-Trans
  actions:
    - theme: brand
      text: 快速开始
      link: /components/readme.html
    - theme: alt
      text: GITEE
      link: https://gitee.com/fhs-opensource/easy_trans
  
features:
- title: 字典翻译
  details: 将 gender code 0 转换为男/女
- title: 外键翻译
  details: userId/idCardNo(身份证号码-唯一键场景)转换为userName
- title: 枚举翻译
  details: Status.OPEN 转换为打开。
---
<style>

.com-box-f{padding: 1em 1em; padding-bottom: 30px; text-align: center;}
.com-box-f h2{font-size: 30px; color: #000; font-weight: 400;}
.com-box{display: flex; flex-wrap: wrap; width: 100%; margin-bottom: 50px; justify-content: flex-start;}
.com-box a{display: block; flex: 0 0 13%; margin: 5px; cursor: pointer; border: 0px #ddd solid;}
.com-box a{line-height: 75px;}
.com-box a img{transition: all 0.2s; vertical-align: middle; min-width: 60%; max-width: 100%; max-height: 100%;}
.com-box a img:hover{transform: scale(1.05, 1.05);}
.com-box-you a img:hover{transform: none;}


.com-box-you a{flex: 0 0 14.5%; line-height: 60px; height: 60px; margin: 10px;}
.com-box-you a img{min-width: 60%; max-width: 85%; vertical-align: middle; max-height: 100%;}
.center{
  text-align: center;
  max-width: 1152px;
}
</style>


<div  style="display: flex;max-width: 1152px;justify-content: center;margin: 0 auto;">
<div class="com-box-f s-width">
					<div class="s-fenge"></div>
					<br>
					<h2 style="padding-bottom: 50px;">
						Dromara 成员项目
					</h2>
					<div class="com-box com-box-you">
						<a href="https://gitee.com/dromara/TLog" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/tlog.png" msg="一个轻量级的分布式日志标记追踪神器，10分钟即可接入，自动对日志打标签完成微服务的链路追踪" src="https://oss.dev33.cn/sa-token/link/tlog.png" style="">
						</a>
						<a href="https://gitee.com/dromara/liteFlow" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/liteflow.png" msg="轻量，快速，稳定，可编排的组件式流程引擎" src="https://oss.dev33.cn/sa-token/link/liteflow.png" style="">
						</a>
						<a href="https://hutool.cn/" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/hutool.jpg" msg="小而全的Java工具类库，使Java拥有函数式语言般的优雅，让Java语言也可以“甜甜的”。" src="https://oss.dev33.cn/sa-token/link/hutool.jpg" style="">
						</a>
						<a href="https://sa-token.cc/" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/sa-token.png" msg="一个轻量级 java 权限认证框架，让鉴权变得简单、优雅！" src="https://oss.dev33.cn/sa-token/link/sa-token.png" style="">
						</a>
						<a href="https://gitee.com/dromara/hmily" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/hmily.png" msg="高性能一站式分布式事务解决方案。" src="https://oss.dev33.cn/sa-token/link/hmily.png" style="">
						</a>
						<a href="https://gitee.com/dromara/Raincat" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/raincat.png" msg="强一致性分布式事务解决方案。" src="https://oss.dev33.cn/sa-token/link/raincat.png" style="">
						</a>
						<a href="https://gitee.com/dromara/myth" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/myth.png" msg="可靠消息分布式事务解决方案。" src="https://oss.dev33.cn/sa-token/link/myth.png" style="">
						</a>
						<a href="https://cubic.jiagoujishu.com/" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/cubic.png" msg="一站式问题定位平台，以agent的方式无侵入接入应用，完整集成arthas功能模块，致力于应用级监控，帮助开发人员快速定位问题" src="https://oss.dev33.cn/sa-token/link/cubic.png" style="">
						</a>
						<a href="https://maxkey.top/" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/maxkey.png" msg="业界领先的身份管理和认证产品" src="https://oss.dev33.cn/sa-token/link/maxkey.png" style="">
						</a>
						<a href="http://forest.dtflyx.com/" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/forest-logo.png" msg="Forest能够帮助您使用更简单的方式编写Java的HTTP客户端" nf="" src="https://oss.dev33.cn/sa-token/link/forest-logo.png" style="">
						</a>
						<a href="https://jpom.top/" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/jpom.png" msg="一款简而轻的低侵入式在线构建、自动部署、日常运维、项目监控软件" src="https://oss.dev33.cn/sa-token/link/jpom.png" style="">
						</a>
						<a href="https://su.usthe.com/" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/sureness.png" msg="面向 REST API 的高性能认证鉴权框架" src="https://oss.dev33.cn/sa-token/link/sureness.png" style="">
						</a>
						<a href="https://easy-es.cn/" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/easy-es2.png" msg="傻瓜级ElasticSearch搜索引擎ORM框架" src="https://oss.dev33.cn/sa-token/link/easy-es2.png" style="">
						</a>
						<a href="https://gitee.com/dromara/northstar" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/northstar_logo.png" msg="Northstar盈富量化交易平台" src="https://oss.dev33.cn/sa-token/link/northstar_logo.png" style="">
						</a>
						<a href="https://dromara.gitee.io/fast-request/" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/fast-request.gif" msg="Idea 版 Postman，为简化调试API而生" src="https://oss.dev33.cn/sa-token/link/fast-request.gif" style="">
						</a>
						<a href="https://www.jeesuite.com/" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/mendmix.png" msg="开源分布式云原生架构一站式解决方案" src="https://oss.dev33.cn/sa-token/link/mendmix.png" style="">
						</a>
						<a href="https://gitee.com/dromara/koalas-rpc" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/koalas-rpc2.png" msg="企业生产级百亿日PV高可用可拓展的RPC框架。" src="https://oss.dev33.cn/sa-token/link/koalas-rpc2.png" style="">
						</a>
						<a href="https://async.sizegang.cn/" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/gobrs-async.png" msg="配置极简功能强大的异步任务动态编排框架" src="https://oss.dev33.cn/sa-token/link/gobrs-async.png" style="">
						</a>
						<a href="https://dynamictp.cn/" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/dynamic-tp.png" msg="基于配置中心的轻量级动态可监控线程池" src="https://oss.dev33.cn/sa-token/link/dynamic-tp.png" style="">
						</a>
						<a href="https://www.x-easypdf.cn" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/x-easypdf.png" msg="一个用搭积木的方式构建pdf的框架（基于pdfbox）" src="https://oss.dev33.cn/sa-token/link/x-easypdf.png" style="">
						</a>
						<a href="http://dromara.gitee.io/image-combiner" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/image-combiner.png" msg="一个专门用于图片合成的工具，没有很复杂的功能，简单实用，却不失强大" src="https://oss.dev33.cn/sa-token/link/image-combiner.png" style="">
						</a>
						<a href="https://www.herodotus.cn/" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/dante-cloud2.png" msg="Dante-Cloud 是一款企业级微服务架构和服务能力开发平台。" src="https://oss.dev33.cn/sa-token/link/dante-cloud2.png" style="">
						</a>
						<a href="http://www.mtruning.club" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/go-view.png" msg="低代码数据可视化开发平台" src="https://oss.dev33.cn/sa-token/link/go-view.png" style="">
						</a>
						<a href="https://tangyh.top/" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/lamp-cloud.png" msg="微服务中后台快速开发平台，支持租户(SaaS)模式、非租户模式" src="https://oss.dev33.cn/sa-token/link/lamp-cloud.png" style="">
						</a>
						<a href="https://www.redisfront.com/" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/redis-front.png" msg="RedisFront 是一款开源免费的跨平台 Redis 桌面客户端工具, 支持单机模式, 集群模式, 哨兵模式以及 SSH 隧道连接, 可轻松管理Redis缓存数据." src="https://oss.dev33.cn/sa-token/link/redis-front.png" style="">
						</a>
						<a href="https://www.yuque.com/u34495/mivcfg" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/electron-egg.png" msg="一个入门简单、跨平台、企业级桌面软件开发框架" src="https://oss.dev33.cn/sa-token/link/electron-egg.png" style="">
						</a>
						<a href="https://gitee.com/dromara/open-capacity-platform" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/open-capacity-platform.jpg" msg="简称ocp是基于Spring Cloud的企业级微服务框架(用户权限管理，配置中心管理，应用管理，....)" src="https://oss.dev33.cn/sa-token/link/open-capacity-platform.jpg" style="">
						</a>
						<a href="http://easy-trans.fhs-opensource.top/" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/easy_trans.png" msg="Easy-Trans 一个注解搞定数据翻译,减少30%SQL代码量" src="https://oss.dev33.cn/sa-token/link/easy_trans.png" style="">
						</a>
						<a href="https://gitee.com/dromara/neutrino-proxy" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/neutrino-proxy.svg" msg="一款基于 Netty 的、开源的内网穿透神器。" src="https://oss.dev33.cn/sa-token/link/neutrino-proxy.svg" style="">
						</a>
						<a href="https://chatgpt.cn.obiscr.com/" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/chatgpt.png" msg="一个支持在 JetBrains 系列 IDE 上运行的 ChatGPT 的插件。" src="https://oss.dev33.cn/sa-token/link/chatgpt.png" style="">
						</a>
						<a href="https://gitee.com/dromara/zyplayer-doc" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/zyplayer-doc.png" msg="zyplayer-doc是一款适合团队和个人使用的WIKI文档管理工具，同时还包含数据库文档、Api接口文档。" src="https://oss.dev33.cn/sa-token/link/zyplayer-doc.png" style="">
						</a>
						<a href="https://gitee.com/dromara/payment-spring-boot" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/payment-spring-boot.png" msg="最全最好用的微信支付V3 Spring Boot 组件。" src="https://oss.dev33.cn/sa-token/link/payment-spring-boot.png" style="">
						</a>
						<a href="https://www.j2eefast.com/" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/j2eefast.png" msg="J2eeFAST 是一个致力于中小企业 Java EE 企业级快速开发平台,我们永久开源!" src="https://oss.dev33.cn/sa-token/link/j2eefast.png" style="">
						</a>
						<a href="https://gitee.com/dromara/data-compare" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/dataCompare.png" msg="数据库比对工具：hive 表数据比对，mysql、Doris 数据比对，实现自动化配置进行数据比对，避免频繁写sql 进行处理，低代码(Low-Code) 平台" src="https://oss.dev33.cn/sa-token/link/dataCompare.png" style="">
						</a>
						<a href="https://gitee.com/dromara/open-giteye-api" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/open-giteye-api.svg" msg="giteye.net 是专为开源作者设计的数据图表服务工具类站点，提供了包括 Star 趋势图、贡献者列表、Gitee指数等数据图表服务。" src="https://oss.dev33.cn/sa-token/link/open-giteye-api.svg" style="">
						</a>
						<a href="https://gitee.com/dromara/RuoYi-Vue-Plus" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/RuoYi-Vue-Plus.png" msg="后台管理系统 重写 RuoYi-Vue 所有功能 集成 Sa-Token + Mybatis-Plus + Jackson + Xxl-Job + SpringDoc + Hutool + OSS 定期同步" src="https://oss.dev33.cn/sa-token/link/RuoYi-Vue-Plus.png" style="">
						</a>
						<a href="https://gitee.com/dromara/RuoYi-Cloud-Plus" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/RuoYi-Cloud-Plus.png" msg="微服务管理系统 重写RuoYi-Cloud所有功能 整合 SpringCloudAlibaba Dubbo3.0 Sa-Token Mybatis-Plus MQ OSS ES Xxl-Job Docker 全方位升级 定期同步" src="https://oss.dev33.cn/sa-token/link/RuoYi-Cloud-Plus.png" style="">
						</a>
						<a href="https://gitee.com/dromara/stream-query" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/stream-query.png" msg="允许完全摆脱 Mapper 的 mybatis-plus 体验！封装 stream 和 lambda 操作进行数据返回处理。" src="https://oss.dev33.cn/sa-token/link/stream-query.png" style="">
						</a>
						<a href="https://wind.kim/" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/sms4j.png" msg="短信聚合工具，让发送短信变的更简单。" src="https://oss.dev33.cn/sa-token/link/sms4j.png" style="">
						</a>
						<a href="https://cloudeon.top/" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/cloudeon.png" msg="简化kubernetes上大数据集群的运维管理" src="https://oss.dev33.cn/sa-token/link/cloudeon.png" style="">
						</a>
						<a href="https://github.com/dromara/hodor" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/hodor.png" msg="Hodor是一个专注于任务编排和高可用性的分布式任务调度系统。" src="https://oss.dev33.cn/sa-token/link/hodor.png" style="">
						</a>
						<a href="http://nsrule.com/" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/test-hub.png" msg="流程编排，插件驱动，测试无限可能" src="https://oss.dev33.cn/sa-token/link/test-hub.png" style="">
						</a>
						<a href="https://gitee.com/dromara/disjob" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/disjob-2.png" msg="Disjob是一个分布式的任务调度框架" src="https://oss.dev33.cn/sa-token/link/disjob-2.png" style="">
						</a>
						<a href="https://gitee.com/dromara/binlog4j" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/Binlog4j.png" msg="轻量级 Mysql Binlog 客户端, 提供宕机续读, 高可用集群等特性" src="https://oss.dev33.cn/sa-token/link/Binlog4j.png" style="">
						</a>
						<a href="https://gitee.com/dromara/yft-design" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/yft-design.png" msg="基于 Canvas 的开源版 创客贴 支持导出json，svg, image文件。" src="https://oss.dev33.cn/sa-token/link/yft-design.png" style="">
						</a>
						<a href="https://gitee.com/dromara/spring-file-storage" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/file4j.png" msg="在 SpringBoot 中通过简单的方式将文件存储到 本地、阿里云 OSS、腾讯云 COS、七牛云 Kodo等" src="https://oss.dev33.cn/sa-token/link/file4j.png" style="">
						</a>
						<a href="https://wemq.nicholasld.cn/" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/wemq.png" msg="开源、高性能、安全、功能强大的物联网调试和管理解决方案。" src="https://oss.dev33.cn/sa-token/link/wemq.png" style="">
						</a>
						<a href="https://gitee.com/dromara/mayfly-go" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/mayfly-go.png" msg="web 版 linux(终端[终端回放] 文件 脚本 进程 计划任务)、数据库（mysql postgres）、redis(单机 哨兵 集群)、mongo 统一管理操作平台" src="https://oss.dev33.cn/sa-token/link/mayfly-go.png" style="">
						</a>
						<a href="https://akali.yomahub.com/" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/akali.png" msg="Akali(阿卡丽)，轻量级本地化热点检测/降级框架，10秒钟即可接入使用！大流量下的神器" src="https://oss.dev33.cn/sa-token/link/akali.png" style="">
						</a>
						<a href="https://gitee.com/dromara/dbswitch" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/dbswitch.png" msg="异构数据库迁移同步(搬家)工具。" src="https://oss.dev33.cn/sa-token/link/dbswitch.png" style="">
						</a>
						<a href="https://gitee.com/dromara/easyAi" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/easyAI.png" msg="Java 傻瓜式 AI 框架。" src="https://oss.dev33.cn/sa-token/link/easyAI.png" style="">
						</a>
						<a href="https://gitee.com/dromara/mybatis-plus-ext" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/mybatis-plus-ext.png" msg="mybatis-plus 框架的增强拓展包。" src="https://oss.dev33.cn/sa-token/link/mybatis-plus-ext.png" style="">
						</a>
						<a href="https://gitee.com/dromara/dax-pay" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/dax-pay.png" msg="免费开源的支付网关。" src="https://oss.dev33.cn/sa-token/link/dax-pay.png" style="">
						</a>
						<a href="https://gitee.com/dromara/sayOrder" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/sayorder.png" msg="基于easyAi引擎的JAVA高性能，低成本，轻量级智能客服。" src="https://oss.dev33.cn/sa-token/link/sayorder.png" style="">
						</a>
						<a href="https://gitee.com/dromara/mybatis-jpa-extra" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/mybatis-jpa-extra.png" msg="扩展MyBatis JPA支持，简化CUID操作，增强SELECT分页查询" src="https://oss.dev33.cn/sa-token/link/mybatis-jpa-extra.png" style="">
						</a>
						<a href="https://dromara.org/zh/projects/" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/dromara.png" msg="让每一位开源爱好者，体会到开源的快乐。" src="https://oss.dev33.cn/sa-token/link/dromara.png" style="">
						</a>
					</div>
                    <h2 style="padding-bottom: 50px;">
						从Dromara毕业的项目
					</h2>
                   <div class="com-box com-box-you">
						<a href="https://hertzbeat.com/" target="_blank">
							<img class="lazy" data-original="https://oss.dev33.cn/sa-token/link/hertzbeat-brand.svg" msg="易用友好的云监控系统" src="https://oss.dev33.cn/sa-token/link/hertzbeat-brand.svg" style="">
						</a>
                    </div>
					<div style="height: 10px; clear: both;"></div>
				</div>

</div>
